package nl.profit4cloud.neo4jrest.entities;

import lombok.Getter;
import lombok.Setter;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.List;

@Getter
@Setter
@NodeEntity
public class Director {
    @Id
    private String name;

    @Relationship(type = "DIRECTED")
    private List<Movie> movies;
}
