package nl.profit4cloud.neo4jrest.entities;

import lombok.Getter;
import lombok.Setter;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.List;

@Getter
@Setter
@NodeEntity
public class Actor {
    @Id
    private String name;

    @Relationship(type = "ACTED_IN")
    private List<Movie> movies;
}
