package nl.profit4cloud.neo4jrest.entities;

import lombok.Getter;
import lombok.Setter;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Property;
import org.neo4j.ogm.annotation.Required;

@Getter
@Setter
@NodeEntity
public class User {
    @Id
    @Property("userId")
    private String id;

    @Required
    private String name;
}
