package nl.profit4cloud.neo4jrest.entities;

import lombok.Getter;
import lombok.Setter;
import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.Required;
import org.neo4j.ogm.annotation.StartNode;

@Getter
@Setter
@RelationshipEntity(type = "RATED")
public class MovieRating {
    @StartNode
    private User user;

    @EndNode
    private Movie movie;

    @Required
    private double rating;
}
