package nl.profit4cloud.neo4jrest.entities;

import lombok.Getter;
import lombok.Setter;
import org.neo4j.ogm.annotation.*;

import java.util.List;

@Getter
@Setter
@NodeEntity
public class Movie {
    @Id
    @Property("movieId")
    private String id;

    @Required
    private String title;

    @Relationship(type = "IN_GENRE")
    private List<Genre> genres;
}
