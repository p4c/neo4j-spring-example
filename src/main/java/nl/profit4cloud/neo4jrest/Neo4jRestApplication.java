package nl.profit4cloud.neo4jrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Neo4jRestApplication {
    public static void main(String[] args) {
        SpringApplication.run(Neo4jRestApplication.class, args);
    }
}
