package nl.profit4cloud.neo4jrest.controllers;

import lombok.RequiredArgsConstructor;
import nl.profit4cloud.neo4jrest.entities.Actor;
import nl.profit4cloud.neo4jrest.repositories.ActorRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/actors")
@RequiredArgsConstructor
public class ActorController {
    private final ActorRepository actorRepository;

    @GetMapping("/{name}")
    public ResponseEntity<List<Actor>> findActorsByName(@PathVariable String name) {
        return ResponseEntity.ok(actorRepository.findByName(name));
    }
}
