package nl.profit4cloud.neo4jrest.controllers;

import lombok.RequiredArgsConstructor;
import nl.profit4cloud.neo4jrest.entities.User;
import nl.profit4cloud.neo4jrest.repositories.UserRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {
    private final UserRepository userRepository;

    @GetMapping("/{id}")
    public ResponseEntity<User> getUserById(@PathVariable String id) {
        return userRepository.findById(id).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping
    public ResponseEntity<List<User>> findByName(@RequestParam String name) {
        return ResponseEntity.ok(userRepository.findByNameContainingIgnoreCase(name));
    }
}
