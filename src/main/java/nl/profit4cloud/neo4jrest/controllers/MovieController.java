package nl.profit4cloud.neo4jrest.controllers;

import lombok.RequiredArgsConstructor;
import nl.profit4cloud.neo4jrest.entities.Actor;
import nl.profit4cloud.neo4jrest.entities.Movie;
import nl.profit4cloud.neo4jrest.entities.User;
import nl.profit4cloud.neo4jrest.repositories.MovieRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/movies")
@RequiredArgsConstructor
public class MovieController {
    private final MovieRepository movieRepository;

    @GetMapping
    public ResponseEntity<List<Movie>> find(@RequestParam String title) {
        return ResponseEntity.ok(movieRepository.findByTitleContainingIgnoreCase(title));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Movie> getById(@PathVariable String id) {
        return movieRepository.findById(id).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/{id}/actors")
    public ResponseEntity<List<Actor>> getMovieActors(@PathVariable String id) {
        return ResponseEntity.ok(movieRepository.getActors(id));
    }

    @GetMapping("/{id}/fans")
    public ResponseEntity<List<User>> findMovieFans(@PathVariable String id) {
        return ResponseEntity.ok(movieRepository.findFans(id));
    }

    @GetMapping("/{id}/avgRating")
    public ResponseEntity<Double> avgRating(@PathVariable String id) {
        return ResponseEntity.ok(movieRepository.getAverageRating(id));
    }
}
