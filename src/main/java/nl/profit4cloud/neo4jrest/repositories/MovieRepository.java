package nl.profit4cloud.neo4jrest.repositories;

import nl.profit4cloud.neo4jrest.entities.Actor;
import nl.profit4cloud.neo4jrest.entities.Movie;
import nl.profit4cloud.neo4jrest.entities.User;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;

import java.util.List;
import java.util.Optional;

public interface MovieRepository extends Neo4jRepository<Movie, String> {
    @Override
    Optional<Movie> findById(String id);

    List<Movie> findByTitleContainingIgnoreCase(String title);

    @Query("MATCH (m1:Movie { movieId: {0} })<-[r1:ACTED_IN]-(a:Actor)-[r2:ACTED_IN]->(m2:Movie) RETURN a, r1, m1, r2, m2 ORDER BY a.name ASC")
    List<Actor> getActors(String movieId);

    @Query("MATCH (:Movie { movieId: {0} })<-[r:RATED]-() RETURN avg(r.rating)")
    double getAverageRating(String movieId);

    @Query(value = "MATCH (:Movie { movieId: {0} })<-[r:RATED]-(u:User) WHERE r.rating > 4 RETURN u ORDER BY u.id ASC")
    List<User> findFans(String movieId);
}
