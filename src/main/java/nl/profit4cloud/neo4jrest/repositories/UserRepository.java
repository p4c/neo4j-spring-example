package nl.profit4cloud.neo4jrest.repositories;

import nl.profit4cloud.neo4jrest.entities.User;
import org.springframework.data.neo4j.repository.Neo4jRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends Neo4jRepository<User, String> {
    @Override
    Optional<User> findById(String id);

    List<User> findByNameContainingIgnoreCase(String name);
}
