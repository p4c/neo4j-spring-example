package nl.profit4cloud.neo4jrest.repositories;

import nl.profit4cloud.neo4jrest.entities.Actor;
import org.springframework.data.neo4j.repository.Neo4jRepository;

import java.util.List;

public interface ActorRepository extends Neo4jRepository<Actor, String> {
    List<Actor> findByName(String name);
}
