# spring-neo4j-example
This repository contains the sample application for this [blog article](https://www.profit4cloud.nl/blog/introduction-to-graph-databases-using-neo4j-part-2/). It exposes a simple REST API backed by a Neo4j database.

## Running the application
The application uses the Movie recommendations database of the [Neo4j sandbox](https://sandbox.neo4j.com/) environment.

To run, log in to the sandbox and launch a project based on the 'Recommendations' dataset. When it's running, navigate to the connection details tab and copy Username, Password and Bolt URL to application.yml:

```
spring:
  data:
    neo4j:
      uri: bolt://[ip]:[port]
      username: neo4j
      password: ********
``` 

After that, you can run the application using maven:

```
mvn spring-boot:run
```
